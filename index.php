<?php

// Массив ошибок
$error = [
    1 => "В имени должны содержаться только кирилические символы без цифр и лишних знаков.",
    2 => "В фамилии должна использоваться только кирилица без цифр и лишних знаков.",
    3 => "Email уже занят.",
    4 => "Превышен лимит символов в пункте о себе. Максимум 512 символов."
];

// Переменная содержащая код ошибки
$code = 0;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    // Дериктория загруженных фотографий
    $targetDir = "uploads/";

    // Переменные для присоединения к базе данных
    $servername = "localhost";
    $username = "newuser";
    $db_password = "password";
    $db = 'contact_form';
    
    // Подключение к базе данных
    $mysqli = mysqli_connect($servername, $username, $db_password, $db);
    if (!$mysqli) {
        die("Connection failed: " . mysqli_connect_error());
    }

    // Проверка на кирилические символы и запись имени
    if (preg_match("/^[а-я А-Я]+$/u", $_REQUEST['first_name']) && $code === 0){
        $first_name = trim($_REQUEST['first_name']);
    } else {
        if ($code === 0)
            $code = 1;
    }
    
    // Проверка на кирилические символы и запись фамилии
    if (preg_match("/^[а-я А-Я]+$/u", $_REQUEST['last_name']) && $code === 0){
        $last_name = trim($_REQUEST['last_name']);
    } else {
        if ($code === 0)
            $code = 2;
    }

    // Запись Email и проверка на занятость
    $email = trim($_REQUEST['email']);
    $check = $mysqli -> query("SELECT * FROM customers WHERE email='$email'");
    if ($check -> num_rows !== 0 && $code === 0)
        $code = 3;

    // Запись пола
    $gender = trim($_REQUEST['gender']);

    // Запись направления и его сериализации для удобного хранения
    if (!empty($_REQUEST['typeWork'])) {
        $typeWork = serialize($_REQUEST['typeWork']);
    } else {
        $typeWork = serialize("Не выбрано");
    }

    // Запись занятости
    $workTime = $_REQUEST['workTime'];

    // Запись пункта о себе и проверка на превышения 512 символов
    if (!empty($_REQUEST['about'])) {
        if(mb_strlen($_REQUEST['about'],'UTF-8') < 512)
            $about = $_REQUEST['about'];
        else
            if ($code === 0)
                $code = 4;
    } else {
        $about = "Нет";
    }

    // Запись наличии фотографии и перемещение в папку uploads
    if(!empty($_FILES["photo"]["name"])) {
        $fileName = basename($_FILES["photo"]["name"]);
        $targetFilePath = $targetDir . $fileName;
        
        move_uploaded_file($_FILES["photo"]["tmp_name"], $targetFilePath);    
    } else {
        $fileName = "Нет";
    }

    // Если при вводе никаких ошибок не возникало, то идет запись в базу данных
    if ($code === 0)
        // Запись в базу данных
        $user_is_created = $mysqli->query("INSERT INTO customers (first_name, last_name, email, gender, typeWork, workTime, about, photo) VALUES ('$first_name', '$last_name', '$email', '$gender', '$typeWork', '$workTime', '$about', '$fileName')");
}

require 'index.html';

?>