-- MariaDB dump 10.19  Distrib 10.6.3-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: contact_form
-- ------------------------------------------------------
-- Server version	10.6.3-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `typeWork` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `workTime` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (92,'Елена','Крузина','crusin.elen@mail.ru','Женщина','a:1:{i:0;s:6:\"DevOps\";}','Гибкий график','Я специалист DevOps направления. Буду рада у вас работать!','1000s_d_850.jpg'),(93,'Юрий','Грудин','yura-grud@mail.ru','Мужчина','a:3:{i:0;s:9:\"Front-end\";i:1;s:8:\"Back-end\";i:2;s:6:\"DevOps\";}','Полный день','Лучший специалист во всей России. Согласен работать за большую зарплату.','755039177971965.jpg'),(94,'Анастасия','Ярулина','nastya_love@mail.ru','Женщина','a:1:{i:0;s:9:\"Front-end\";}','Неполный день','Я только начинаю свой путь программиста. В вашей компании я хочу набраться опыта. Примите меня пожалуйста!!!','kas.jpg'),(95,'Алеша','Хмурый','vashe.pofig@mail.ru','Мужчина','s:19:\"Не выбрано\";','Любая занятость','Да мне не важно. Захочу буду работать. Захочу не буду. Мне вобще пофиг.','Нет'),(96,'Лол','Зачем','loooool@lololo.ru','Женщина','s:19:\"Не выбрано\";','Любая занятость','Хахахахахаха кто вы такие? Я вас не знаю! Пойду тролировать других, лоооооооол!','Нет');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-01 23:13:51
